export interface Translatable {

  updateLanguage(val: string): void;
}
