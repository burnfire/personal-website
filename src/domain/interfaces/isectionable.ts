export interface ISectionable {
  gotoSection(section: string): void;
}
