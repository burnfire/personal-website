import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CiripitorComponent } from './ciripitor.component';

describe('CiripitorComponent', () => {
  let component: CiripitorComponent;
  let fixture: ComponentFixture<CiripitorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CiripitorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CiripitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
