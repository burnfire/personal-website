import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabelADRComponent } from './tabel-adr.component';

describe('TabelADRComponent', () => {
  let component: TabelADRComponent;
  let fixture: ComponentFixture<TabelADRComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabelADRComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelADRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
