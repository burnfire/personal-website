import {PersonalInfo} from '../domain/contact-info';

export const petruInfo: PersonalInfo = new PersonalInfo('Ștefănescu Petru-Teodor', new Date(2001, 2, 23), [
  {appName: 'Yahoo', appId: 'stefanescupetru@yahoo.com', appUrl: 'mailto:stefanescupetru@yahoo.com'},
  {appName: 'LinkedIn', appId: 'Petru Ștefănescu', appUrl: 'https://www.linkedin.com/in/petru-teodor-ștefănescu-7aa204136'}
], [
  {appName: 'Skype', appId: 'stefanescupetru'},
  {appName: 'Discord', appId: 'burnfire#4473'}
]);
